from django.conf.urls import url
from . import views


app_name = 'dd3'
urlpatterns = [
    url('^$', views.index, name='index'),
]
